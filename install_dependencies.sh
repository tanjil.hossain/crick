#!/bin/bash
set -e

# Navigate to the directory of your Node.js application
cd /home/ubuntu/cricket-connection
sudo su
sudo chmod -R 777 /home/ubuntu/cricket-connection

# Install project dependencies
npm install
