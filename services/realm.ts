import { App } from "realm-web";

export const dbConnection = {
  appId: "application-0-zvhozup",
  appUrl:
    "https://realm.mongodb.com/groups/66726ebf66ddff1251bb45ad/apps/667be5c4e75b0dbbfc5c7d36",
  baseUrl: "https://realm.mongodb.com",
  dataSourceName: "mongodb-atlas",
  dbName: "cricconnection",
};
const appId = dbConnection.appId;
const app = new App({ id: appId });

export { app };
