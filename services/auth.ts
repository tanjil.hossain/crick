import * as Realm from "realm-web"; // Import all as Realm
import { app, dbConnection } from "./realm";

// Define the additional user information type
interface UserInfo {
  name: string;
  email: string;
  postcode: string;
  password: string;
}

// Define the return type for the sign-up and log-in functions
interface AuthResponse {
  success: boolean;
  user?: Realm.User; // User object from Realm on successful login
  profile?: UserProfile | null; // Profile object with additional user information
  error?: string; // Error message on failure
}

// Define the additional user information type
interface UserProfile {
  _id: string;
  firstName: string;
  lastName: string;
  age: number;
}

// Sign-up function with additional fields
export const signUp = async (userInfo: UserInfo): Promise<AuthResponse> => {
  try {
    // Register the user with email and password
    await app.emailPasswordAuth.registerUser({
      email: userInfo.email,
      password: userInfo.password,
    });

    // Log in the user immediately after registration to get the user object
    const credentials = Realm.Credentials.emailPassword(
      userInfo.email,
      userInfo.password,
    );
    const user = await app.logIn(credentials);

    // Insert the additional user information into the custom user data collection
    const userId = user.id;
    const mongodb = app.currentUser?.mongoClient(dbConnection.dataSourceName);
    const collection = mongodb?.db(dbConnection.dbName).collection("users");

    await collection?.insertOne({
      userIdFromProvider: userId,
      ...userInfo,
    });

    return { success: true, user };
  } catch (err) {
    const errorMessage = getErrorMessage(err);
    return { success: false, error: errorMessage };
  }
};

// Log in function with fetching additional user information
export const logIn = async (
  email: string,
  password: string,
): Promise<AuthResponse> => {
  try {
    const credentials = Realm.Credentials.emailPassword(email, password);
    const user = await app.logIn(credentials);

    // Fetch additional user information from the UserProfile collection
    const mongodb = app.currentUser?.mongoClient("mongodb-atlas");
    const collection = mongodb
      ?.db(dbConnection.dbName)
      .collection<UserProfile>("users");
    const profile = await collection?.findOne({ _id: user.id });

    // Store the token in localStorage
    if (user.accessToken) {
      localStorage.setItem("realmToken", user.accessToken);
    }
    return { success: true, user, profile };
  } catch (err) {
    const errorMessage = getErrorMessage(err);
    return { success: false, error: errorMessage };
  }
};

// Log out function
export const logOut = async (): Promise<AuthResponse> => {
  try {
    await app.currentUser?.logOut();
    return { success: true };
  } catch (err) {
    const errorMessage = getErrorMessage(err);
    return { success: false, error: errorMessage };
  }
};

// Check if user is logged in
export const getCurrentUser = (): Realm.User | null => {
  return app.currentUser;
};

// Utility function to get a meaningful error message
const getErrorMessage = (err: unknown): string => {
  if (err instanceof Error) {
    return err.message;
  }
  if (typeof err === "string") {
    return err;
  }
  if (err && typeof err === "object") {
    try {
      return JSON.stringify(err);
    } catch (stringifyError) {
      return "Unknown error occurred";
    }
  }
  return "Unknown error occurred";
};
