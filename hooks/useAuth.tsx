"use client";

import { useRouter } from "next/navigation";
import {
  createContext,
  ReactNode,
  useContext,
  useEffect,
  useState,
} from "react";
import * as Realm from "realm-web";
import { app, dbConnection } from "../services/realm";

interface AuthContextType {
  user: Realm.User | null;
  loading: boolean;
  login: (email: string, password: string) => Promise<AuthResponse>;
  logout: () => Promise<void>;
  userProfile: UserProfile | null;
  signup: (userInfo: UserProfile) => Promise<AuthResponse>;
}

const AuthContext = createContext<AuthContextType>({
  user: null,
  loading: true,
  login: async () => ({ success: false, error: "Not implemented" }),
  logout: async () => {},
  userProfile: null,
  signup: async () => ({ success: false, error: "Not implemented" }),
});

interface UserProfile {
  _id?: string;
  name: string;
  email: string;
  postcode: string;
  password: string;
  userIdFromProvider?: string;
}

interface AuthResponse {
  success: boolean;
  user?: Realm.User;
  profile?: UserProfile | null;
  error?: string;
}

export const useAuth = () => useContext(AuthContext);

export const AuthProvider = ({ children }: { children: ReactNode }) => {
  const [user, setUser] = useState<Realm.User | null>(null);
  const [userProfile, setUserProfile] = useState<UserProfile | null>(null);
  const [loading, setLoading] = useState(true);
  const router = useRouter();

  useEffect(() => {
    const checkAuth = async () => {
      const storedToken = localStorage.getItem("id");
      if (storedToken) {
        try {
          const mongodb = app.currentUser?.mongoClient("mongodb-atlas");
          const collection = mongodb
            ?.db(dbConnection.dbName)
            .collection("users");
          const profile: UserProfile = await collection?.findOne({
            userIdFromProvider: storedToken,
          });
          setUserProfile(profile || null);
        } catch (error) {
          console.error("Re-authentication failed:", error);
        }
      }
      setLoading(false);
    };

    checkAuth();
  }, []);

  const login = async (
    email: string,
    password: string,
  ): Promise<AuthResponse> => {
    try {
      setLoading(true);

      const credentials = Realm.Credentials.emailPassword(email, password);
      const user = await app.logIn(credentials);
      const mongodb = app.currentUser?.mongoClient("mongodb-atlas");
      const collection = mongodb?.db(dbConnection.dbName).collection("users");
      const profile: UserProfile = await collection?.findOne({
        userIdFromProvider: user.id,
      });

      // Store the token in localStorage
      if (user.id) {
        localStorage.setItem("id", user.id);
        document.cookie = `currentUser=${user.id}; path=/; expires=${new Date(new Date().getTime() + 1 * 24 * 60 * 60 * 1000).toUTCString()}`;
      }
      setUser(user);
      setUserProfile(profile || null);
      setLoading(false);
      return { success: true, user, profile };
    } catch (error) {
      setLoading(false);
      return { success: false, error: "error.message" || "Login failed" };
    }
  };
  const signup = async (userInfo: UserProfile): Promise<AuthResponse> => {
    try {
      // Register the user with email and password
      await app.emailPasswordAuth.registerUser({
        email: userInfo.email,
        password: userInfo.password,
      });

      // Log in the user immediately after registration to get the user object
      const credentials = Realm.Credentials.emailPassword(
        userInfo.email,
        userInfo.password,
      );
      const user = await app.logIn(credentials);
      // Insert the additional user information into the custom user data collection
      const userId = user.id;
      const mongodb = app.currentUser?.mongoClient(dbConnection.dataSourceName);
      const collection = mongodb?.db(dbConnection.dbName).collection("users");

      const { password, ...restUserInfo } = userInfo;
      await collection?.insertOne({
        userIdFromProvider: userId,
        ...restUserInfo,
      });

      return { success: true, user };
    } catch (err) {
      return { success: false, error: "Failed to signup!" };
    }
  };

  const logout = async () => {
    try {
      await app.currentUser?.logOut();
      setUser(null);
      setUserProfile(null);
      router.push("/login");
      localStorage.removeItem("id");
      document.cookie =
        "currentUser=; path=/; expires=Thu, 01 Jan 1970 00:00:00 UTC;";
      setLoading(false);
    } catch (e) {
      setLoading(false);
    }
  };

  return (
    <AuthContext.Provider
      value={{ user, userProfile, loading, login, logout, signup }}
    >
      {children}
    </AuthContext.Provider>
  );
};
