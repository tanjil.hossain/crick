import * as Realm from "realm-web";
import { useCollection } from "./useCollection";
import { dbConnection } from "@/services/realm";

export function useUsers() {
  const usersCollection = useCollection({
    cluster: dbConnection.dataSourceName,
    db: dbConnection.dbName,
    collection: "users",
  });

  const getUsers = async () => {
    try {
      const users = await usersCollection?.find();

      return users;
    } catch (e) {
      console.log(e);
    }
  };

  return {
    getUsers,
  };
}
