"use client";

import { Button } from "@/components/ui/button";
import { useAuth } from "@/hooks/useAuth";

const Report = () => {
  const { userProfile, loading, logout } = useAuth();
  if (loading) return <div>loading...</div>;
  const handleLogout = async () => {
    await logout();
  };
  return (
    <div>
      <h1>Protected Page</h1>
      <p>Welcome, {userProfile?.name}</p>
      <Button
        onClick={handleLogout}
        variant={"destructive"}
        className="rounded-xl"
      >
        Logout
      </Button>
    </div>
  );
};

export default Report;
