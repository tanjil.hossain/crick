import Layout from "../page";

const Blog = () => {
  return (
    <Layout>
      <div className="bg-custom-gradient h-96">Blog page</div>
    </Layout>
  );
};

export default Blog;
