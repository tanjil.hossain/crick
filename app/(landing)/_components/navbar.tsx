"use client";

import { Button } from "@/components/ui/button";
import Image from "next/image";
import Link from "next/link";
import { usePathname, useRouter } from "next/navigation";

const Navbar: React.FC = () => {
  const pathname = usePathname();
  const router = useRouter();

  const navItems: { href: string; label: string }[] = [
    { href: "/", label: "Home" },
    { href: "/about", label: "About Us" },
    { href: "/contact", label: "Contact Us" },
    { href: "/blog", label: "Media" },
  ];

  const getLinkClass = (href: string) =>
    pathname === href ? "font-bold text-blue-500" : "";

  const handleLoginClick = () => {
    router.push("/login");
  };
  const handleRegisterClick = () => {
    router.push("/register");
  };

  return (
    <div className="flex justify-between bg-white px-9 py-5">
      <div className="flex items-center">
        <ul className="flex justify-between space-x-9">
          {navItems.map((item) => (
            <li key={item.href}>
              <Link
                href={item.href}
                className={getLinkClass(item.href)}
                prefetch
              >
                {item.label}
              </Link>
            </li>
          ))}
        </ul>
      </div>
      <div className="-ml-20">
        <Image src="/cricIcon.svg" alt="Cricket Icon" height={44} width={176} />
      </div>
      <div className="flex space-x-2">
        <Button
          className="w-24 rounded-full border-[#E41D38] text-[#E41D38]"
          variant={"outline"}
          onClick={handleLoginClick}
        >
          Login
        </Button>
        <Button
          onClick={handleRegisterClick}
          className="w-24 rounded-full text-white"
          variant="destructive"
        >
          Register
        </Button>
      </div>
    </div>
  );
};

export default Navbar;
