"use client";

import { zodResolver } from "@hookform/resolvers/zod";
import { useForm } from "react-hook-form";
import { z } from "zod";

import { Button } from "@/components/ui/button";
import {
  Form,
  FormControl,
  FormField,
  FormItem,
  FormMessage,
} from "@/components/ui/form";
import { Input } from "@/components/ui/input";
import { Separator } from "@/components/ui/separator";
import { toast } from "@/components/ui/use-toast";
import { useAuth } from "@/hooks/useAuth";
import { Eye, EyeOff, Loader } from "lucide-react";
import Image from "next/image";
import Link from "next/link";
import { useRouter } from "next/navigation";
import { useState } from "react";

const FormSchema = z.object({
  email: z.string().email().min(2, {
    message: "Username must be at least 2 characters.",
  }),
  password: z
    .string()
    .min(6, {
      message: "Password minimum length must be 6",
    })
    .max(8, {
      message: "Password maximum length not moren than 8",
    }),
});

const LoginForm = () => {
  const [isPasswordShow, setIsPasswordShow] = useState(false);
  const router = useRouter();
  const { login, loading } = useAuth();
  const form = useForm<z.infer<typeof FormSchema>>({
    resolver: zodResolver(FormSchema),
    defaultValues: {
      email: "",
      password: "",
    },
  });

  const onSubmit = async (data: z.infer<typeof FormSchema>) => {
    // const result = await signUp(data.email, data.password, {
    //   firstName: "Admin",
    //   lastName: "Admin",
    // });
    const result = await login(data.email, data.password);
    if (result.success) {
      router.prefetch("/dashboard");
      router.push("/dashboard");
      toast({
        variant: "success",
        description: "Login Successful!",
      });
    } else {
      toast({
        variant: "error",
        description: "Invalid username or password!",
      });
    }
  };

  const handleTogglePasswordShow = () => setIsPasswordShow((p) => !p);

  return (
    <div className="space-y-4">
      <Form {...form}>
        <form onSubmit={form.handleSubmit(onSubmit)} className="space-y-6">
          <FormField
            control={form.control}
            name="email"
            render={({ field }) => (
              <FormItem>
                <FormControl>
                  <Input
                    className="w-full rounded-xl"
                    placeholder="Enter email id"
                    {...field}
                  />
                </FormControl>

                <FormMessage />
              </FormItem>
            )}
          />
          <div className="relative">
            <FormField
              control={form.control}
              name="password"
              render={({ field }) => (
                <FormItem>
                  <FormControl>
                    <Input
                      className="w-full rounded-xl"
                      placeholder="Enter Password"
                      type={isPasswordShow ? "text" : "password"}
                      {...field}
                    />
                  </FormControl>

                  <FormMessage />
                </FormItem>
              )}
            />
            <button
              type="button"
              onClick={handleTogglePasswordShow}
              className="absolute right-5 top-2 text-muted-foreground"
            >
              {isPasswordShow ? <Eye /> : <EyeOff />}
            </button>
            <p className="mt-1 cursor-pointer text-right text-sm text-slate-600 hover:underline">
              Forgot Password
            </p>
          </div>

          <Button
            type="submit"
            variant={"destructive"}
            className="w-full rounded-xl"
          >
            {loading && <Loader className="mr-2 h-4 w-4 animate-spin" />}
            Login
          </Button>
        </form>
      </Form>

      <Separator className="my-4 bg-slate-300" />

      <div className="space-y-5">
        <Button className="w-full space-x-2 rounded-xl" variant={"outline"}>
          <Image
            src={"/images/login/logo_google.svg"}
            alt="google logo"
            width={20}
            height={20}
          />
          <span className="font-semibold text-[#E41D38]">
            Login with Google
          </span>
        </Button>
        <Button className="w-full space-x-2 rounded-xl" variant={"outline"}>
          <Image
            src={"/images/login/logo_facebook.svg"}
            alt="google logo"
            width={20}
            height={20}
          />
          <span className="font-semibold text-[#E41D38]">
            Login with Facebook
          </span>
        </Button>
      </div>

      <div className="flex justify-center gap-2">
        <span className="text-gray-600">Do not have an account?</span>{" "}
        <Link
          href={"/register"}
          className="cursor-pointer text-blue-600 hover:underline"
        >
          Register
        </Link>
      </div>
    </div>
  );
};

export default LoginForm;
