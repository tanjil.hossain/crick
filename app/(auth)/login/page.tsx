"use client";

import { Tabs, TabsContent, TabsList, TabsTrigger } from "@/components/ui/tabs";
import Image from "next/image";
import { useState } from "react";
import LoginForm from "./_components/login-form";

type UserType = "student" | "coach";

const Login = () => {
  const [userType, setUserType] = useState<UserType>("student");
  const handleUserType = (type: UserType) => {
    setUserType(type);
  };

  return (
    <div className="grid h-screen grid-cols-1 md:grid-cols-2">
      <div className="relative order-2 bg-gradient-to-tl from-violet-900 to-rose-600 md:order-1">
        <div className="mx-auto flex max-w-[600px] flex-col items-center space-y-4 p-5 text-white">
          <Image
            width={500}
            height={500}
            src={`/images/login/pitch-player.png`}
            alt="player image"
            className="order-3 mx-auto md:order-1 md:mt-[120px]"
          />

          <Image
            width={200}
            height={200}
            src={`/images/login/ball-star.png`}
            alt="rectangle image"
            className="invisible absolute right-0 top-5 md:visible"
          />

          <h1 className="order-1 text-center text-2xl font-bold uppercase italic md:order-2 md:not-italic">
            {userType === "coach"
              ? "Coaching Excellence Unveiled"
              : "Empower the Cricket Player in You"}
          </h1>
          <p className="order-2 max-w-md text-center md:order-3">
            {userType === "coach"
              ? `Welcome back! Log in to access personalized cricket coaching and
            resources. Take the next step in your cricket journey with ease`
              : "Embark on a journey of cricketing enlightenment as you step into the world of student onboarding with Cricket Connections."}
          </p>
        </div>
      </div>

      <div className="order-1 mx-auto flex max-w-[438px] flex-col items-center space-y-4 p-5 py-5 md:order-2">
        <Image width={200} height={200} src={`/images/logo.png`} alt="logo" />
        <h1 className="bg-gradient-to-r from-[#7d2a35] to-[#E41D38] bg-clip-text text-[40px] font-bold text-transparent">
          Login
        </h1>
        <p className="md:text-md text-center text-sm uppercase">
          Access your account as a student or coach to continue your cricket
          journey.
        </p>

        <Tabs
          onValueChange={(value) => handleUserType(value as UserType)}
          defaultValue="student"
          className="w-full"
        >
          <TabsList className="mx-auto grid w-1/2 grid-cols-2 rounded-full">
            <TabsTrigger
              className="rounded-full uppercase data-[state=active]:bg-blue-800 data-[state=active]:text-white"
              value="student"
            >
              Student
            </TabsTrigger>
            <TabsTrigger
              className="rounded-full uppercase data-[state=active]:bg-blue-800 data-[state=active]:text-white"
              value="coach"
            >
              Coach
            </TabsTrigger>
          </TabsList>
          <TabsContent value="student">
            <LoginForm />
          </TabsContent>
          <TabsContent value="coach">
            <LoginForm />
          </TabsContent>
        </Tabs>
      </div>
    </div>
  );
};

export default Login;
