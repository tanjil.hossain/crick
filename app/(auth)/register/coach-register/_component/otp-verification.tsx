"use client";
import { Button } from "@/components/ui/button";
import {
  InputOTP,
  InputOTPGroup,
  InputOTPSlot,
} from "@/components/ui/input-otp";
import { Separator } from "@/components/ui/separator";
import { REGEXP_ONLY_DIGITS_AND_CHARS } from "input-otp";
import Image from "next/image";
import Link from "next/link";
import { FC } from "react";
import TwoColumnWrapper from "../../_component/two-column-wrapper";

interface OtpVerificationProps {
  handleStep: (value: number) => void;
}

const OtpVerification: FC<OtpVerificationProps> = ({ handleStep }) => {
  return (
    <TwoColumnWrapper>
      <div className="space-y-5">
        <Image
          width={200}
          height={200}
          className="mx-auto hidden md:block"
          src={`/images/logo.png`}
          alt="logo"
        />
        <div className="space-y-5">
          <div className="space-y-2">
            <div className="text-center text-xl font-extrabold">
              <span className="bg-gradient-to-r from-pink-500 to-violet-500 bg-clip-text text-transparent">
                OTP Verification
              </span>
            </div>
            <p className="text-center text-sm uppercase">
              An OTP has been sent to your Email ID
            </p>
          </div>

          <div className="flex items-center justify-center">
            <InputOTP maxLength={6} pattern={REGEXP_ONLY_DIGITS_AND_CHARS}>
              <InputOTPGroup>
                <InputOTPSlot index={0} />
                <InputOTPSlot index={1} />
                <InputOTPSlot index={2} />
                <InputOTPSlot index={3} />
              </InputOTPGroup>
            </InputOTP>
          </div>
        </div>
      </div>

      <div>
        <div className="flex justify-center gap-2 text-sm">
          <span className="text-slate-600">Didnt receive OTP?</span>
          <span className="text-blue-600 hover:underline">Resend</span>
        </div>
        <div className="flex justify-between">
          <Button
            className="border-pink-300"
            variant={"outline"}
            onClick={() => handleStep(0)}
          >
            Prev
          </Button>
          <Button variant={"destructive"} onClick={() => handleStep(2)}>
            Next
          </Button>
        </div>
        <Separator className="my-4" />

        <div className="flex justify-center gap-2 text-sm">
          <span className="text-slate-600">Having Trouble Registering?</span>
          <Link href={"/contact"} className="text-blue-600 hover:underline">
            Contact Us
          </Link>
        </div>
      </div>
    </TwoColumnWrapper>
  );
};

export default OtpVerification;
