import { Trash2 } from "lucide-react";
import Image from "next/image";
import React, { useState } from "react";

const FileUploader = () => {
  const [uploadedFileName, setUploadedFileName] = useState<string | null>(null);

  const handleFileChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    if (event.target.files && event.target.files.length > 0) {
      setUploadedFileName(event.target.files[0].name);
    }
  };
  const handleClearFile = (event: React.MouseEvent) => {
    event.stopPropagation();
    setUploadedFileName(null);
  };
  return (
    <label htmlFor="file-upload" className="cursor-pointer">
      <div className="space-y-4 rounded-3xl border border-dashed border-slate-500 p-5">
        {uploadedFileName ? (
          <Image
            width={30}
            height={30}
            src={`/images/register/file-text.svg`}
            alt="download icon"
            className="mx-auto"
          />
        ) : (
          <Image
            width={30}
            height={30}
            src={`/images/register/download-icon.svg`}
            alt="download icon"
            className="mx-auto"
          />
        )}

        <p className="text-center text-muted-foreground">
          {uploadedFileName
            ? `Uploaded: ${uploadedFileName}`
            : "Browse and choose the files you want to upload from your computer"}
        </p>

        {uploadedFileName ? (
          <div className="flex justify-center">
            <span className="text-sm text-blue-500">Reupload</span>
            <span className="mx-4 h-[1/2] w-[1px] bg-slate-500"></span>
            <Trash2
              onClick={handleClearFile}
              size={20}
              className="text-slate-600 cursor-pointer"
            />
          </div>
        ) : (
          <Image
            width={30}
            height={30}
            src={`/images/register/plus-icon.svg`}
            alt="plus icon"
            className="mx-auto rounded bg-rose-600"
          />
        )}

        <input
          id="file-upload"
          type="file"
          className="hidden"
          onChange={handleFileChange}
        />
      </div>
    </label>
  );
};

export default FileUploader;
