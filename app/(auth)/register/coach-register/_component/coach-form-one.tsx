"use client";

import { zodResolver } from "@hookform/resolvers/zod";
import { useForm } from "react-hook-form";
import { z } from "zod";

import { Button } from "@/components/ui/button";
import { Checkbox } from "@/components/ui/checkbox";
import {
  Form,
  FormControl,
  FormField,
  FormItem,
  FormMessage,
} from "@/components/ui/form";
import { Input } from "@/components/ui/input";
import { toast } from "@/components/ui/use-toast";
import { useAuth } from "@/hooks/useAuth";
import { Eye, EyeOff, Loader } from "lucide-react";
import Image from "next/image";
import Link from "next/link";
import { useRouter } from "next/navigation";
import { FC, useState } from "react";
import FormWrapper from "../../_component/form-wrapper";

const FormSchema = z.object({
  name: z.string().min(2, {
    message: "Name must be at least 2 characters.",
  }),
  email: z.string().email({
    message: "Invalid email address.",
  }),
  postcode: z.string().min(2, {
    message: "Postcode must be at least 2 characters.",
  }),
  password: z
    .string()
    .min(6, {
      message: "Password must be at least 6 characters long.",
    })
    .max(8, {
      message: "Password must not exceed 8 characters.",
    }),
  confirmPassword: z
    .string()
    .min(6, {
      message: "Password must be at least 6 characters long.",
    })
    .max(8, {
      message: "Password must not exceed 8 characters.",
    }),
});

interface CoachRegistrationFormProps {
  handleStep: (value: number) => void;
}

const CoachRegistrationForm: FC<CoachRegistrationFormProps> = ({
  handleStep,
}) => {
  const [isPasswordShow, setIsPasswordShow] = useState(false);
  const router = useRouter();
  const { signup, loading } = useAuth();
  const form = useForm<z.infer<typeof FormSchema>>({
    resolver: zodResolver(FormSchema),
    defaultValues: {
      name: "",
      email: "",
      postcode: "",
      password: "",
      confirmPassword: "",
    },
  });

  const onSubmit = async (data: z.infer<typeof FormSchema>) => {
    const { confirmPassword, ...rest } = data;
    const result = await signup(rest);
    if (result.success) {
      handleStep(1);
      router.prefetch("/dashboard");
      router.push("/dashboard");
      toast({
        variant: "success",
        description: "Login Successful!",
      });
    } else {
      // for demo(remote it later)
      handleStep(1);
      toast({
        variant: "error",
        description: "Invalid username or password!",
      });
    }
  };

  const handleTogglePasswordShow = () => setIsPasswordShow((p) => !p);

  return (
    <FormWrapper>
      <div className="mx-auto max-w-md space-y-3">
        <Image
          width={200}
          height={200}
          src={`/images/logo.png`}
          alt="player image"
          className="mx-auto"
        />

        <div className="text-center text-2xl font-extrabold">
          <span className="bg-gradient-to-r from-pink-500 to-violet-500 bg-clip-text text-transparent">
            Register As A Coach
          </span>
        </div>
        <p className="text-center text-sm uppercase">
          join our cricket community to share your expertise and mentor aspiring
          players.
        </p>
        <Form {...form}>
          <form onSubmit={form.handleSubmit(onSubmit)} className="space-y-6">
            <FormField
              control={form.control}
              name="name"
              render={({ field }) => (
                <FormItem>
                  <FormControl>
                    <Input
                      className="w-full rounded-xl"
                      placeholder="Name"
                      {...field}
                    />
                  </FormControl>

                  <FormMessage />
                </FormItem>
              )}
            />

            <FormField
              control={form.control}
              name="email"
              render={({ field }) => (
                <FormItem>
                  <FormControl>
                    <Input
                      className="w-full rounded-xl"
                      placeholder="Email Id"
                      {...field}
                    />
                  </FormControl>

                  <FormMessage />
                </FormItem>
              )}
            />
            <FormField
              control={form.control}
              name="postcode"
              render={({ field }) => (
                <FormItem>
                  <FormControl>
                    <Input
                      className="w-full rounded-xl"
                      placeholder="Post Code"
                      {...field}
                    />
                  </FormControl>

                  <FormMessage />
                </FormItem>
              )}
            />
            <div className="relative">
              <FormField
                control={form.control}
                name="password"
                render={({ field }) => (
                  <FormItem>
                    <FormControl>
                      <Input
                        className="w-full rounded-xl"
                        placeholder="Set Password"
                        type={isPasswordShow ? "text" : "password"}
                        {...field}
                      />
                    </FormControl>

                    <FormMessage />
                  </FormItem>
                )}
              />
              <button
                type="button"
                onClick={handleTogglePasswordShow}
                className="absolute right-5 top-3 text-muted-foreground"
              >
                {isPasswordShow ? <Eye size={16} /> : <EyeOff size={16} />}
              </button>
            </div>

            <div className="relative">
              <FormField
                control={form.control}
                name="confirmPassword"
                render={({ field }) => (
                  <FormItem>
                    <FormControl>
                      <Input
                        className="w-full rounded-xl"
                        placeholder="Confirm Password"
                        type={isPasswordShow ? "text" : "password"}
                        {...field}
                      />
                    </FormControl>

                    <FormMessage />
                  </FormItem>
                )}
              />
              <button
                type="button"
                onClick={handleTogglePasswordShow}
                className="absolute right-5 top-3 text-muted-foreground"
              >
                {isPasswordShow ? <Eye size={16} /> : <EyeOff size={16} />}
              </button>
            </div>

            <div className="flex items-center space-x-2">
              <Checkbox id="terms" />
              <label
                htmlFor="terms"
                className="text-sm leading-none text-slate-500 peer-disabled:cursor-not-allowed peer-disabled:opacity-70"
              >
                Accept terms and conditions
              </label>
            </div>

            <div className="flex items-center space-x-2">
              <Checkbox id="confirm" />
              <label
                htmlFor="confirm"
                className="text-sm leading-none text-slate-500 peer-disabled:cursor-not-allowed peer-disabled:opacity-70"
              >
                I confirm that the information provided is accurate.
              </label>
            </div>

            <Button
              type="submit"
              variant={"destructive"}
              className="w-full rounded-xl"
            >
              {loading && <Loader className="mr-2 h-4 w-4 animate-spin" />}
              Sign Up
            </Button>
          </form>
        </Form>
        <div className="flex justify-center gap-2">
          <Button className="rounded-xl" variant={"outline"}>
            <Image
              src={"/images/login/logo_google.svg"}
              alt="google logo"
              width={20}
              height={20}
            />
          </Button>
          <Button className="rounded-xl" variant={"outline"}>
            <Image
              src={"/images/login/logo_facebook.svg"}
              alt="google logo"
              width={20}
              height={20}
            />
          </Button>
        </div>
        <div className="text-center text-sm">
          <span className="mr-1 text-slate-500">Already have an account?</span>
          <Link className="text-blue-500 underline" href={"/login"}>
            Login
          </Link>
        </div>
      </div>
    </FormWrapper>
  );
};

export default CoachRegistrationForm;
