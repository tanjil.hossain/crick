"use client";

import { zodResolver } from "@hookform/resolvers/zod";
import { useForm } from "react-hook-form";
import { z } from "zod";

import { Button } from "@/components/ui/button";
import {
  Form,
  FormControl,
  FormField,
  FormItem,
  FormMessage,
} from "@/components/ui/form";
import { Input } from "@/components/ui/input";
import {
  Select,
  SelectContent,
  SelectItem,
  SelectTrigger,
  SelectValue,
} from "@/components/ui/select";
import { Separator } from "@/components/ui/separator";
import { toast } from "@/components/ui/use-toast";
import { useAuth } from "@/hooks/useAuth";
import Image from "next/image";
import Link from "next/link";
import { useRouter } from "next/navigation";
import { FC } from "react";
import TwoColumnWrapper from "../../_component/two-column-wrapper";

const FormSchema = z.object({
  phone: z.string().min(11, {
    message: "phone number must be at least 11 characters.",
  }),
  age: z.number({
    message: "Invalid email address.",
  }),
  gender: z.string(),
});

interface CoachRegistrationFormTwoProps {
  handleStep: (value: number) => void;
}

const CoachRegistrationFormTwo: FC<CoachRegistrationFormTwoProps> = ({
  handleStep,
}) => {
  const router = useRouter();
  const { login, loading } = useAuth();
  const form = useForm<z.infer<typeof FormSchema>>({
    resolver: zodResolver(FormSchema),
    defaultValues: {
      phone: "",
      gender: "",
    },
  });

  const onSubmit = async (data: z.infer<typeof FormSchema>) => {
    const result = { success: false };
    if (result.success) {
      handleStep(1);

      router.prefetch("/dashboard");
      router.push("/dashboard");
      toast({
        variant: "success",
        description: "Login Successful!",
      });
    } else {
      // for demo(remote it later)
      handleStep(1);
      toast({
        variant: "error",
        description: "Invalid username or password!",
      });
    }
  };

  return (
    <TwoColumnWrapper>
      <div className="flex h-[100%] flex-col justify-between">
        <div className="mx-auto max-w-md space-y-3">
          <Image
            width={200}
            height={200}
            src={`/images/logo.png`}
            alt="player image"
            className="mx-auto"
          />

          <div className="text-center text-2xl font-extrabold">
            <span className="bg-gradient-to-r from-pink-500 to-violet-500 bg-clip-text text-transparent">
              Register As A Coach
            </span>
          </div>
          <p className="text-center text-sm uppercase">
            join our cricket community to share your expertise and mentor
            aspiring players.
          </p>
          <Form {...form}>
            <form onSubmit={form.handleSubmit(onSubmit)} className="space-y-6">
              <FormField
                control={form.control}
                name="phone"
                render={({ field }) => (
                  <FormItem>
                    <FormControl>
                      <Input
                        className="w-full rounded-xl"
                        placeholder="Phone No."
                        {...field}
                      />
                    </FormControl>

                    <FormMessage />
                  </FormItem>
                )}
              />

              <FormField
                control={form.control}
                name="age"
                render={({ field }) => (
                  <FormItem>
                    <FormControl>
                      <Input
                        className="w-full rounded-xl"
                        placeholder="Age"
                        {...field}
                      />
                    </FormControl>

                    <FormMessage />
                  </FormItem>
                )}
              />

              <FormField
                control={form.control}
                name="gender"
                render={({ field }) => (
                  <FormItem>
                    <Select
                      onValueChange={field.onChange}
                      defaultValue={field.value}
                    >
                      <FormControl>
                        <SelectTrigger>
                          <SelectValue placeholder="Gender" />
                        </SelectTrigger>
                      </FormControl>
                      <SelectContent>
                        <SelectItem value="male">Male</SelectItem>
                        <SelectItem value="female">Female</SelectItem>
                        <SelectItem value="others">Others</SelectItem>
                      </SelectContent>
                    </Select>

                    <FormMessage />
                  </FormItem>
                )}
              />
            </form>
          </Form>
        </div>

        <div>
          <div className="flex justify-between">
            <Button
              onClick={() => handleStep(1)}
              className="border-pink-300"
              variant={"outline"}
            >
              Prev
            </Button>
            <Button variant={"destructive"} onClick={() => handleStep(3)}>
              Next
            </Button>
          </div>
          <Separator className="my-4" />

          <div className="flex justify-center gap-2 text-sm">
            <span className="text-slate-600">Having Trouble Registering?</span>
            <Link href={"/contact"} className="text-blue-600 hover:underline">
              Contact Us
            </Link>
          </div>
        </div>
      </div>
    </TwoColumnWrapper>
  );
};

export default CoachRegistrationFormTwo;
