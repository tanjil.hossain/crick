"use client";

import { useState } from "react";
import CoachRegistrationForm from "./_component/coach-form-one";
import CoachRegistrationFormTwo from "./_component/coach-form-two";
import FileUploadForm from "./_component/file-upload-form";
import OtpVerification from "./_component/otp-verification";

const CoachRegister = () => {
  const [step, setStep] = useState(0);

  const handleStep = (value: number) => {
    setStep(value);
  };
  return (
    <div>
      {step === 0 && <CoachRegistrationForm handleStep={handleStep} />}
      {step === 1 && <OtpVerification handleStep={handleStep} />}
      {step === 2 && <CoachRegistrationFormTwo handleStep={handleStep} />}
      {step === 3 && <FileUploadForm handleStep={handleStep} />}
    </div>
  );
};

export default CoachRegister;
