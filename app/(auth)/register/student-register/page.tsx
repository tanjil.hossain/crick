"use client";
import { useState } from "react";
import AgeVerification from "../_component/age-verification";
import ConfirmationPage from "../_component/confirmation-page";
import OnboardingWrapper from "../_component/onboarding-wrapper";
import StudentOnboarding from "../_component/student-onboarding";

const StudentRegister = () => {
  const [step, setStep] = useState(0);
  const [studentAge, setStudentAge] = useState(0);
  const handleStep = (stepValue: number) => {
    setStep(stepValue);
  };
  const updateStudentAge = (age: number) => {
    setStudentAge(age);
  };
  return (
    <OnboardingWrapper>
      {step === 0 && <StudentOnboarding handleStep={handleStep} />}
      {step === 1 && (
        <AgeVerification
          updateStudentAge={updateStudentAge}
          handleStep={handleStep}
        />
      )}
      {step === 2 && <ConfirmationPage />}
    </OnboardingWrapper>
  );
};

export default StudentRegister;
