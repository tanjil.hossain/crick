"use client";

import { useRouter } from "next/navigation";
import MainOnBoarding from "./_component/main-onboarding";
import OnboardingWrapper from "./_component/onboarding-wrapper";

const RegistrationOnBoarding = () => {
  const router = useRouter();

  return (
    <OnboardingWrapper>
      <MainOnBoarding />
    </OnboardingWrapper>
  );
};

export default RegistrationOnBoarding;
