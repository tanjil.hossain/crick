import Image from "next/image";
import { FC, ReactNode } from "react";

interface FormWrapperProps {
  children: ReactNode;
}

const FormWrapper: FC<FormWrapperProps> = ({ children }) => {
  return (
    <div className="grid h-screen grid-cols-1 md:grid-cols-2">
      <div className="relative order-2 space-y-5 bg-gradient-to-br from-[#db1724] to-[#2747D7] p-5 md:order-1 md:p-20">
        <Image
          width={200}
          height={200}
          className="absolute right-0 top-5 hidden md:block"
          src={`/images/register/ball-star.png`}
          alt="logo"
        />
        <div className="space-y-4 rounded-3xl bg-white p-10">
          
          <Image
            width={50}
            height={50}
            src={"/images/register/vector.svg"}
            alt="quote"
            className="mx-auto p-2 bg-yellow-300 rounded-xl"
          />
          <p className="text-center text-slate-600">
            Lorem ipsum dolor sit amet consectetur adipisicing elit. Architecto
            quod deleniti asperiores suscipit aliquam quia aperiam quo nulla
            error omnis aut ab adipisci ad, dolorem nesciunt alias reiciendis
            mollitia. Pariatur.
          </p>

          <div className="flex flex-col items-center">
            <Image
              width={50}
              height={50}
              src={"/images/register/profile.png"}
              alt="profile"
              className="rounded-full "
            />
            <p className="font-semibold text-slate-600">Janina Roosen</p>
          </div>
        </div>

        <div className="rounded-3xl bg-gray-500/40 p-5 text-slate-800">
          Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse
          varius enim in eros elementum tristique. Duis cursus, mi quis viverra
          ornare, eros dolor interdum nulla, ut commodo diam libero vitae erat.
        </div>

        <div className="space-y-5 text-white">
          <h1 className="text-center text-lg font-bold uppercase">
            Coaching Excellence Unveiled
          </h1>
          <p className="mx-auto max-w-md text-center text-sm">
            Step into the world of cricket coaching excellence through the
            testimonials of seasoned mentors who have transformed the game.
          </p>
        </div>
      </div>
      <div className="order-1 flex flex-col justify-between p-5 md:order-2">
        {children}
      </div>
    </div>
  );
};

export default FormWrapper;
