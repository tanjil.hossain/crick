import Image from "next/image";
import React, { FC } from "react";

interface TwoColumnWrapperProps {
  children: React.ReactNode;
  imageSrc?: string;
}

const TwoColumnWrapper: FC<TwoColumnWrapperProps> = ({
  children,
  imageSrc,
}) => {
  return (
    <div className="grid h-screen grid-cols-1 md:grid-cols-2">
      <div className="relative order-2 bg-gradient-to-br from-[#db1724] to-[#2747D7] text-white md:order-1">
        <div className="flex h-[70%] items-center justify-center">
          <Image
            width={400}
            height={400}
            className="visible -inset-10"
            src={imageSrc || `/images/register/helmate.png`}
            alt="logo"
          />
        </div>

        <Image
          width={200}
          height={200}
          className="absolute right-0 top-5 hidden md:block"
          src={`/images/register/ball-star.png`}
          alt="logo"
        />
        <div className="space-y-5">
          <h1 className="text-center text-lg font-bold uppercase">
            Coaching Excellence Unveiled
          </h1>
          <p className="mx-auto max-w-md text-center text-sm">
            Step into the world of cricket coaching excellence through the
            testimonials of seasoned mentors who have transformed the game.
          </p>
        </div>
      </div>
      <div className="order-1 flex flex-col justify-between p-5 md:order-2">
        {children}
      </div>
    </div>
  );
};

export default TwoColumnWrapper;
