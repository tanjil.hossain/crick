import { Button } from "@/components/ui/button";
import Image from "next/image";
import Link from "next/link";
import { useRouter } from "next/navigation";

const MainOnBoarding = () => {
  const router = useRouter();
  return (
    <>
      <div className="flex items-center justify-center">
        <Image
          width={200}
          height={200}
          className="hidden md:block"
          src={`/images/logo.png`}
          alt="logo"
        />

        <Image
          width={150}
          height={150}
          className="visible md:hidden"
          src={`/images/register/helmate.png`}
          alt="logo"
        />
      </div>
      <h1 className="text-center text-xl font-extrabold uppercase md:text-4xl">
        Choose your journey
      </h1>

      <div className="mx-auto flex max-w-md items-center gap-5">
        <div className="flex flex-col items-center gap-4">
          <Image
            width={20}
            height={20}
            alt="red ball"
            src={"/images/register/red-ball.png"}
          />
          <h1 className="text-center text-sm text-slate-600">
            Create Your Account With Us
          </h1>
        </div>
        <div className="flex flex-col items-center gap-4">
          <Image
            width={20}
            height={20}
            alt="white ball"
            src={"/images/register/white-ball.png"}
          />
          <h1 className="text-center text-sm text-slate-600">
            Explore Wide-Range of Profiles
          </h1>
        </div>

        <div className="flex flex-col items-center gap-4">
          <Image
            width={20}
            height={20}
            alt="white ball"
            src={"/images/register/white-ball.png"}
          />
          <h1 className="text-center text-sm text-slate-600">
            Book Training Programs
          </h1>
        </div>
        <div className="flex flex-col items-center gap-4">
          <Image
            width={20}
            height={20}
            alt="white ball"
            src={"/images/register/white-ball.png"}
          />
          <h1 className="text-center text-sm text-slate-600">
            Train And Improve
          </h1>
        </div>
      </div>

      <p className="mx-auto max-w-md text-center text-sm text-slate-600 md:text-base">
        Welcome to Cricket Connections! Before we get started, let&apos;s
        determine the journey you&apos;d like to embark on. Are you here to
        share your expertise as a coach, or are you ready to hone your skills as
        a student? Select your desired path below to begin your cricketing
        adventure:
      </p>

      <div className="space-y-4">
        <div className="flex flex-col justify-center gap-4 md:flex-row">
          <Button
            onClick={() => router.push("/register/coach-register")}
            className="rounded-xl"
            variant="destructive"
          >
            I&apos;m Here To Coach
          </Button>
          <Button
            onClick={() => router.push("/register/student-register")}
            className="rounded-xl"
            variant="destructive"
          >
            I&apos;m Here To Learn
          </Button>
        </div>
        <div className="text-center">
          <Link href={"/contact"} className="text-blue-600">
            Contact Us
          </Link>
        </div>
      </div>
    </>
  );
};

export default MainOnBoarding;
