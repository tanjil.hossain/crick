"use client";

import * as React from "react";

interface AgeProgressProps {
  progressValue: number;
}

export const AgeGenerationProgress: React.FC<AgeProgressProps> = ({
  progressValue,
}) => {
  return (
    <div className="flex w-96 flex-col justify-center gap-2">
      <div className="flex items-center justify-between">
        <p className="text-gray-600">Generating Your Age</p>
        <p className="text-gray-600">{progressValue}%</p>
      </div>

      <div className="relative h-1 w-full overflow-hidden rounded-full bg-gray-300">
        <div
          className="absolute left-0 top-0 h-full bg-gradient-to-r from-blue-500 via-purple-500 to-red-500 transition-all duration-500 ease-in-out"
          style={{ width: `${progressValue}%` }}
        />
      </div>
    </div>
  );
};
