"use client";
import { Button } from "@/components/ui/button";
import Image from "next/image";
import { FC } from "react";

interface StudentBoardingProps {
  handleStep: (value: number) => void;
}

const StudentOnboarding: FC<StudentBoardingProps> = ({ handleStep }) => {
  return (
    <div className="flex flex-col gap-10">
      <div className="flex items-center justify-center">
        <Image
          width={200}
          height={200}
          className="hidden md:block"
          src={`/images/logo.png`}
          alt="logo"
        />

        <Image
          width={150}
          height={150}
          className="visible md:hidden"
          src={`/images/register/helmate.png`}
          alt="logo"
        />
      </div>
      <h1 className="mx-auto max-w-3xl text-center text-base font-bold uppercase md:text-3xl">
        Smoothen Your Onboarding by Parental Consent for Minors
      </h1>

      <p className="mx-auto max-w-xl text-center text-sm text-slate-600 md:text-base">
        Before proceeding with the registration process, please be aware that if
        you are under the age of 18, parental consent is required to use our
        platform. This involves providing your parent or guardian&apos;s email
        address, where an OTP (One-Time Password) will be sent for verification
        purposes. By continuing with the registration process, you acknowledge
        and agree to obtain parental consent before accessing our services.
      </p>

      <div className="space-y-4">
        <div className="flex flex-col justify-center gap-4 md:flex-row">
          <Button
            onClick={() => handleStep(1)}
            className="rounded-xl"
            variant="destructive"
          >
            Continue Onboarding
          </Button>
        </div>
        <div className="text-center text-slate-600">
          <p>By continuing you are agreeing for the Age Verification</p>
        </div>
      </div>
    </div>
  );
};

export default StudentOnboarding;
