import { Button } from "@/components/ui/button";
import { toast } from "@/components/ui/use-toast";
import Image from "next/image";
import { FC, useState } from "react";
import { AgeGenerationProgress } from "./progress-component";

interface AgeVerificationProps {
  updateStudentAge: (age: number) => void;
  handleStep: (value: number) => void;
}

const AgeVerification: FC<AgeVerificationProps> = ({
  updateStudentAge,
  handleStep,
}) => {
  const [dateOfBirth, setDateOfBirth] = useState({
    day: "",
    month: "",
    year: "",
  });
  const [progress, setProgress] = useState(0);
  const [isLoading, setIsLoading] = useState(false);
  const [age, setAge] = useState<number | null>(null);

  const handleInputChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const { name, value } = e.target;
    setDateOfBirth((prevState) => ({
      ...prevState,
      [name]: value,
    }));
  };

  const calculateAge = (day: string, month: string, year: string) => {
    setProgress(50);
    const birthDate = new Date(`${year}-${month}-${day}`);
    const today = new Date();
    let age = today.getFullYear() - birthDate.getFullYear();
    const monthDiff = today.getMonth() - birthDate.getMonth();
    if (
      monthDiff < 0 ||
      (monthDiff === 0 && today.getDate() < birthDate.getDate())
    ) {
      age--;
    }
    return age;
  };

  const isValidDate = (day: string, month: string, year: string) => {
    const date = new Date(`${year}-${month}-${day}`);
    return (
      date.getDate() === parseInt(day) &&
      date.getMonth() + 1 === parseInt(month) &&
      date.getFullYear() === parseInt(year)
    );
  };

  const handleSubmit = () => {
    if (age !== null) {
      handleStep(2);
      return;
    }

    const { day, month, year } = dateOfBirth;
    if (day && month && year && isValidDate(day, month, year)) {
      setIsLoading(true);
      setProgress(25);

      setTimeout(() => {
        setProgress(66);

        setTimeout(() => {
          const calculatedAge = calculateAge(day, month, year);
          setProgress(100);

          setTimeout(() => {
            updateStudentAge(calculatedAge);
            setAge(calculatedAge);
            setIsLoading(false);
          }, 1000);
        }, 2000); // Shorter timeout for 66% progress
      }, 1000); // Adjusted timeout for initial progress
    } else {
      toast({
        variant: "error",
        description: "Please enter a valid date of birth!",
      });
    }
  };

  return (
    <div className="flex flex-col gap-5 p-8 text-center">
      <Image
        width={200}
        height={200}
        className="mx-auto hidden md:block"
        src={`/images/logo.png`}
        alt="logo"
      />
      <div className="text-2xl font-extrabold">
        <span className="bg-gradient-to-r from-[#233FD2] to-[#E41D38] bg-clip-text text-transparent">
          Age Verification
        </span>
      </div>

      <p className="description mt-2 text-gray-600">
        Ensuring safety and compliance. Thank you for verifying your age.
      </p>
      <div className="input-container mt-4 flex justify-center gap-2">
        <input
          type="text"
          name="day"
          placeholder="DD"
          value={dateOfBirth.day}
          onChange={handleInputChange}
          className="input w-12 rounded border p-2 text-center"
        />
        <input
          type="text"
          name="month"
          placeholder="MM"
          value={dateOfBirth.month}
          onChange={handleInputChange}
          className="input w-12 rounded border p-2 text-center"
        />
        <input
          type="text"
          name="year"
          placeholder="YYYY"
          value={dateOfBirth.year}
          onChange={handleInputChange}
          className="input w-16 rounded border p-2 text-center"
        />
      </div>
      {isLoading && (
        <div className="mt-2 flex justify-center">
          <AgeGenerationProgress progressValue={progress} />
        </div>
      )}
      {!isLoading && age !== null && (
        <div className="mx-auto mt-2 flex items-center justify-center gap-2 rounded-lg border p-4">
          <span className="text-xl font-bold text-[#444444]">YOUR AGE</span>
          <div className="flex h-12 w-12 items-center justify-center rounded-lg bg-blue-600 text-xl font-bold text-white">
            {age}
          </div>
        </div>
      )}
      <Button
        variant="destructive"
        onClick={handleSubmit}
        className="submit-button mx-auto mt-6 w-48 rounded-full px-4 py-2 text-white"
      >
        Continue
      </Button>
    </div>
  );
};

export default AgeVerification;
