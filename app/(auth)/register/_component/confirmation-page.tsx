"use client";
import { Button } from "@/components/ui/button";
import Image from "next/image";
import { FC } from "react";

interface ConfirmationPageProps {}

const ConfirmationPage: FC<ConfirmationPageProps> = () => {
  return (
    <div className="flex flex-col gap-10">
      <div className="flex items-center justify-center">
        <Image
          width={200}
          height={200}
          className="hidden md:block"
          src={`/images/logo.png`}
          alt="logo"
        />

        <Image
          width={150}
          height={150}
          className="visible md:hidden"
          src={`/images/register/helmate.png`}
          alt="logo"
        />
      </div>
      <h1 className="mx-auto max-w-3xl text-center text-base font-bold uppercase md:text-3xl">
        Thank you for registering, Welcome to Cricket Connections!
      </h1>

      <p className="mx-auto max-w-xl text-center text-sm text-slate-600 md:text-base">
        We&apos;sre thrilled to have you join our community of cricket
        enthusiasts. Continue your onboarding journey to personalize your
        experience and discover everything our platform has to offer.
        Alternatively, if you&apos;sre ready to dive right in, click below to
        access your dashboard and start exploring.
      </p>

      <div className="space-y-4">
        <div className="flex flex-col justify-center gap-4 md:flex-row">
          <Button className="rounded-xl" variant="destructive">
            Continue Onboarding
          </Button>
        </div>
      </div>
    </div>
  );
};

export default ConfirmationPage;
