import Image from "next/image";
import { ReactNode } from "react";

const OnboardingWrapper = ({ children }: { children: ReactNode }) => {
  return (
    <div className="h-screen overflow-hidden bg-gradient-to-r from-[#233FD2] to-[#E41D38] p-5 md:p-20">
      <div className="relative items-center space-y-5 rounded-3xl bg-white p-20">
        {children}
        <Image
          width={200}
          height={200}
          className="invisible absolute md:visible md:bottom-0 md:right-5"
          src={`/images/register/helmate.png`}
          alt="logo"
        />
      </div>
    </div>
  );
};

export default OnboardingWrapper;
