#!/bin/bash
set -e

# Navigate to the directory of your Node.js application
cd /home/ubuntu/cricket-connection
sudo su
# start application
pm2 restart 0